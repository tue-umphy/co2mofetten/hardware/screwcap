// clang-format off
use <./threads-scad/threads.scad>;
// clang-format on

/* [Position] */
position = "above"; // ["together", "above","adjacent"]
// only in preview
slice_in_preview = false;

/* [Fixband] */
fixband = "free";           // ["no","fixed","free"]
fixband_length = 80;        // [10:1:100]
fixband_width = 4;          // [1:0.1:20]
fixband_thickness = 0.5;    // [0.01:0.01:1]
fixband_hole_diameter = 15; // [5:0.1:50]

/* [Threads] */
threads_tooth_angle = 55; // [1:1:60]
threads_pitch = 1.5;      // [0:0.1:5]

/* [Female Plug] */
female_plug_length = 9;
female_plug_thread_diameter = 16.3;

/* [Male Plug] */
male_plug_length = 9;
male_plug_diameter = 13.4;

/* [Cap] */
cap_height = 3;
cap_diameter = 25;
cap_inner_radius = cap_diameter / 2 * cos(30);
cap_side_length = cap_diameter * sin(30);
cap_thread_depth = min(cap_height * 0.9, cap_height - 0.5);

/* [Precision] */
$fa = 0.5;
$fs = 0.5;
epsilon = 0.1;

module
FreeFixbandScrewHole()
{
  if (fixband == "free") {
    ScrewHole(outer_diam = female_plug_thread_diameter,
              height = cap_thread_depth,
              tooth_angle = threads_tooth_angle,
              pitch = threads_pitch

              ) children();
  } else {
    children();
  }
}

module
FreeFixbandScrewCap()
{
  translate([ 0, 0, cap_height + fixband_thickness * 2 ])
    ScrewThread(outer_diam = female_plug_thread_diameter,
                height = cap_thread_depth,
                tooth_angle = threads_tooth_angle,
                pitch = threads_pitch);
  translate([ 0, 0, cap_height ])
    cylinder(d = female_plug_thread_diameter, h = fixband_thickness * 2);
  cylinder(d = cap_diameter, h = cap_height, $fn = 6);
}

module
ScrewCap()
{
  difference()
  {
    translate([ 0, 0, cap_height ])
      ScrewThread(outer_diam = female_plug_thread_diameter,
                  height = female_plug_length,
                  tooth_angle = threads_tooth_angle,
                  pitch = threads_pitch);
    translate([ 0, 0, cap_height + -epsilon / 2 ])
      cylinder(d = male_plug_diameter, h = male_plug_length + epsilon);
  }
  FreeFixbandScrewHole() cylinder(d = cap_diameter, h = cap_height, $fn = 6);
}

module
ScrewContainer()
{
  FreeFixbandScrewHole() ScrewHole(outer_diam = female_plug_thread_diameter,
                                   height = female_plug_length,
                                   tooth_angle = threads_tooth_angle,
                                   pitch = threads_pitch,
                                   position = [ 0, 0, cap_height ],
                                   rotation = [ 0, 0, 180 ])
    cylinder(d = cap_diameter, h = female_plug_length + cap_height, $fn = 6);
}

module
AddFixBand()
{
  children();
  translate(fixband == "free" ? [ 0, 0, -fixband_thickness * 2 ] : [ 0, 0, 0 ])
  {
    if (fixband != "no") {
      // the band
      translate([ -fixband_width / 2, cap_inner_radius, 0 ])
        cube([ fixband_width, fixband_length, fixband_thickness ]);
      if (fixband == "fixed") {
        // strengthening the connection
        hull()
        {
          // starting at the cap
          translate(
            [ 0, cap_inner_radius - epsilon / 2, fixband_thickness / 2 ])
            cube([ cap_side_length, epsilon, fixband_thickness ],
                 center = true);
          // ending at the fixband
          translate([
            0,
            epsilon / 2 + cap_inner_radius +
              min(tan(60) * (cap_side_length - fixband_width) / 2,
                  fixband_length / 2),
            fixband_thickness / 2
          ]) cube([ fixband_width, epsilon, fixband_thickness ], center = true);
        }
      } else if (fixband == "free") {
        rotate([ 0, 0, 180 ]) difference()
        {
          fixband_screw_hole_diameter = female_plug_thread_diameter + 0.5;
          hull()
          {
            // the shape for the ring
            cylinder(r = cap_inner_radius - 0.2, h = fixband_thickness);
            // strengthening
            translate([
              0,
              -cap_inner_radius -
                min((fixband_screw_hole_diameter + fixband_width) / 2,
                    fixband_length / 2),
              fixband_thickness / 2
            ]) cube([ fixband_width, epsilon, fixband_thickness ],
                    center = true);
          }
          // the hole for the cable
          translate([ 0, 0, -epsilon / 2 ]) cylinder(
            d = fixband_screw_hole_diameter, h = fixband_thickness + epsilon);
        }
      }
      // the circle
      translate([
        0,
        cap_inner_radius + fixband_length + fixband_hole_diameter / 2 +
          fixband_width,
        0
      ]) difference()
      {
        hull()
        {
          // the shape for the ring
          cylinder(d = fixband_hole_diameter + 2 * fixband_width,
                   h = fixband_thickness);
          // strengthening
          translate([
            0,
            -cap_inner_radius - min((fixband_hole_diameter + fixband_width) / 2,
                                    fixband_length / 2),
            fixband_thickness / 2
          ]) cube([ fixband_width, epsilon, fixband_thickness ], center = true);
        }
        // the hole for the cable
        translate([ 0, 0, -epsilon / 2 ])
          cylinder(d = fixband_hole_diameter, h = fixband_thickness + epsilon);
      }
    }
  }
}

intersection()
{
  union()
  {
    AddFixBand() ScrewCap();
    translate(
      position == "above"
        ? [ 0, 0, (cap_height * 2 + female_plug_length) * 2 ]
        : (position == "adjacent" ? [ (cap_diameter +fixband_hole_diameter+fixband_width)* 1.2  , 0, 0 ] : [
            0,
            0,
            cap_height * 2 +
            female_plug_length + threads_pitch * 0.1
          ])) rotate(position == "adjacent" ? [ 0, 0, 0 ] : [ 0,
                                                              180,
                                                              0 ]) AddFixBand() ScrewContainer();
    if (fixband == "free") {
      translate(
        position == "above"
          ? [ 0, 0, -(cap_height + female_plug_length) ]
          : (position == "adjacent" ? [ -cap_diameter * 1.5, 0, 0 ] : [
              0,
              0,
              -(cap_height + fixband_thickness * 2 + threads_pitch * 0.1)
            ])) FreeFixbandScrewCap();

      translate(position == "above"
                  ? [ 0, 0, (2*male_plug_length+cap_height*2) * 2]
                  : (position == "adjacent"
                       ? [ -(cap_diameter * 1.5)*2, 0, 0 ]
                       : [ 0, 0, male_plug_length + fixband_thickness*2 - threads_pitch * 0.1 + 3*cap_height +(cap_height-cap_thread_depth )]))
        rotate(position == "above"
                 ? [ 180, 0, 180 ]
                 : (position == "adjacent" ? [ 0, 0, 0 ] : [ 180, 0,180]))
          FreeFixbandScrewCap();
    }
  }
  if (slice_in_preview && $preview) {
    translate([ 0, 0, -female_plug_length * 5 ])
      cube((female_plug_length + cap_diameter + fixband_hole_diameter +
            fixband_width) *
           10);
  }
}

